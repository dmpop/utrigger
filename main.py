import machine
from machine import Pin
import time
import sys

led = Pin(16, Pin.OUT)
flashPin = Pin(4, Pin.OUT)
focusPin = Pin(13, Pin.OUT)
triggerPin = Pin(15, Pin.OUT)
button = Pin(12, Pin.IN, Pin.PULL_UP)
adc = machine.ADC(0)

while True:
    focusPin.off()
    sensorValue=adc.read()
    if sensorValue > 500:
        led.off()
        flashPin.off()
        triggerPin.off()
        time.sleep_ms(100)
        flashPin.on()
        triggerPin.on()
    else:
        led.on()
    if not button.value():
        for i in range(5):
            led.off()
            time.sleep_ms(300)
            led.on()
            time.sleep_ms(300)
        sys.exit(20)
