# uTrigger

uTrigger is a MicroPython script that transforms a NodeMCU ESP8266 board into an optical flash and camera trigger. The trigger is designed to work with any flash with a [PC Sync connector](https://northrup.photo/gear-basics/camera-body-features/pc-sync-cord-connector/) and Sony Alpha cameras. However, adapting the script for use with other flashes and cameras is trivial.

## Usage Scenarios

uTrigger has been developed with two scenarios in mind:

- Optical flash trigger that makes it possible to use an external flash with a cameras that doesn't have a hotshoe. This also allows to use older flashes with any modern camera.
- Lightning trigger for photographing lightnings.

With a minimum of tweaking, uTrigger can be adapted for other uses, too. For example, it can be used to trigger the camera when a laser beam is broken.

## Required Components

Bill of Materials:

- ESP8266 board
- Breadboard
- Jumper or pre-cut wires
- MOC3021 optoisolator
- 10KΩ resistor
- Push button
- Female PC Sync connector
- Camera release cable
- USB cable
- Power bank

The PC Sync adapter and the camera release cable must be modified, so they can be connected to the breadboard.

## Instructions

Wire the components as shown in the illustration.

Install [MicroPython](http://micropython.org/) on to NodeMCU.

- Install *esptool*: `sudo pip3 install esptool`
- Install *ampy*: `sudo pip3 install adafruit-ampy`
- Download the latest firmware from the [MicroPython downloads](http://micropython.org/download#esp8266) page
- Erase the flash: `esptool.py --port /dev/ttyUSB0 erase_flash`
- Push the new firmware: `esptool.py --port /dev/ttyUSB0 --baud 460800 write_flash --flash_size=detect 0 esp8266-xxxxxxxx-vx.x.x.bin`

Clone the uTrigger GitLab repository using the `git clone git@gitlab.com:dmpop/utrigger.git` command, and switch to the *utrigger* directory. Connect the NodeMCU board, and run the `ampy --port /dev/ttyUSB0 -b115200 put main.py` command to transfer the *main.py* script to the board. Reset the board. The trigger should be now operational. You can terminate it by pressing the push button.
